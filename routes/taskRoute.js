// It contains all enpoint for our application
const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController.js");

// [SECTION] Routes
// It is responsible for defining or creating endpoints
// All the business logic is done in the controller.

router.get("/viewTasks", (req, res) => {
	// Invokes the "getAllTasks" function from the "taskController.js" file and sends the result back to the client/Postman
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
});

router.post("/addNewTask", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
});

router.delete("/deleteTask/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => {
		res.send(resultFromController);
	});
})

router.put("/updateTask/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// 4. Process a GET request at the "/tasks/specificTask/:id" route using postman to get a specific task.
router.get("/:id", (req, res) => {
	taskController.getSingleTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
});

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// 5. Create a route for changing the status of a task to "complete".
router.patch("/:id/status", (req, res) => {
	taskController.completeTask(req.params.id, req.body).then((resultFromController) => {
		res.send(resultFromController)
	})
})


module.exports = router;